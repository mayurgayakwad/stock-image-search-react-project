import React from "react";
import "../component/imageCss.css";
class Images extends React.Component {
  render() {
    return (
      <>
        <div className="imageDivs" key="div">
          {this.props.data.map((ele) => (
            <img
              className="images"
              key={ele.urls.regular}
              src={ele.urls.regular}
            />
          ))}
        </div>
      </>
    );
  }
}
export default Images;
