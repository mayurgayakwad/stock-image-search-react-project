import "./App.css";
import React from "react";
import Images from "./component/Images";
import Loader from "./component/loader";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      Searching: "Tesla",
      data: null,
      randomPage: 10,
    };
  }

  fechingImages = () => {
    let url = `https://api.unsplash.com/search/photos?page=${this.state.randomPage}&query=${this.state.Searching}&client_id=nhn4Z_RlbGfMIYY4awy49LUDBA09MrGsVyF9IBocmKY&per_page=15`;
    fetch(url)
      .then((res) => res.json())
      .then((data) => this.setState({ data: data.results }));
  };

  componentDidMount() {
    this.fechingImages();
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      prevState.Searching !== this.state.Searching ||
      prevState.randomPage !== this.state.randomPage
    ) {
      this.fechingImages();
    }
  }

  onInputChange = (e) => {
    this.setState({ Searching: e.target.value });
  };

  clickOnNextPage = (e) => {
    this.setState({ randomPage: Math.floor(Math.random() * 50) });
  };

  render() {
    return (
      <>
        <center>
          <input
            key={this.EnterKey}
            onChange={this.onInputChange}
            className="searchInput"
            placeholder="Search Images"
          />
          <button onClick={this.clickOnNextPage} className="searchButton">
            Next Page
          </button>
        </center>
        {this.state.data ? <Images data={this.state.data} /> : <Loader />}
      </>
    );
  }
}

export default App;
